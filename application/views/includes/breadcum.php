<?php if($this->user->log): ?>

<div class="breadcrumbs" id="breadcrumbs">

        <ul class="breadcrumb">

                <li>

                        <i class="ace-icon fa fa-home home-icon"></i>

                        <a href="#">Home</a>

                </li>

                <li class="active"><?= empty($title)?'Escritorio':$title ?></li>
                <li>
		Usuario: <span style="color:blue"><?= @$this->user->nombre ?></span> | 
		Fecha: <span style="color:blue" id="fechaSistema"><?= date("d/m/Y H:i")  ?></span>
                </li>

        </ul><!-- /.breadcrumb -->

</div>

<?php endif ?>