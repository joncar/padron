<style>
	h4{
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
	}
	@media screen and (max-width:480px){
		.dashboards > div .widget-body{
			width:100%;
			overflow:auto;
		}
	}
</style>
<script src="<?= base_url() ?>js/raphael-min.js"></script>
<script src="<?= base_url() ?>js/prettify.min.js"></script>
<script src="<?= base_url().'js/morris.js' ?>"></script>                                
<link rel="stylesheet" href="<?= base_url() ?>css/prettify.min.css">
<link rel="stylesheet" href="<?= base_url().'css/morris.css' ?>">
<div class="row dashboards">
	<div class="col-xs-12 col-md-6">
		<?= $this->load->view('components/cumple_hoy',array(),TRUE,'dashboards') ?>
	</div>	
	<div class="col-xs-12 col-md-6">
		<?= $this->load->view('components/cumple_mes',array(),TRUE,'dashboards') ?>
	</div>	
</div>
<div class="row dashboards">
	<div class="col-xs-12 col-sm-6 col-md-4">
		<?= $this->load->view('components/tendencia_torta',array(),TRUE,'dashboards') ?>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<?= $this->load->view('components/local_torta',array(),TRUE,'dashboards') ?>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<?= $this->load->view('components/situacion_voto_torta',array(),TRUE,'dashboards') ?>
	</div>
</div>