<?php 
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $qry = $this->db->query("
        SELECT
		voto.denominacion as situacion,
		COUNT(cedula) as cant
		FROM padron
		INNER JOIN voto on voto.id = padron.voto
		GROUP BY padron.voto
    ");
?>
<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-pie-chart"></i> Situación de voto</h5>

                <div class="widget-toolbar">
                    

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main">                        
						<div id="situacionGraph"></div>
                    </div>
                </div>
            </div>
</div>

<script>
    var data = <?php
        $data = array();
        foreach($qry->result() as $q){
            $data[] = array('label'=>$q->situacion,'value'=>$q->cant);
        }
        echo json_encode($data);
    ?>;
	Morris.Donut({
	  element: 'situacionGraph',
	  data: data
	});
</script>