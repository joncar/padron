<?php 
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $qry = $this->db->query("
        SELECT 
		semaforos.denominacion as tendencia,
		COUNT(cedula) as cant
		FROM padron
		INNER JOIN semaforos on semaforos.id = padron.semaforos_id
		GROUP BY padron.semaforos_id
    ");
?>
<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-pie-chart"></i> Tendencia</h5>

                <div class="widget-toolbar">
                    

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main">                        
						<div id="tendenciaGraph"></div>
                    </div>
                </div>
            </div>
</div>

<script>
    var data = <?php
        $data = array();
        foreach($qry->result() as $q){
            $data[] = array('label'=>$q->tendencia,'value'=>$q->cant);
        }
        echo json_encode($data);
    ?>;
	Morris.Donut({
	  element: 'tendenciaGraph',
	  data: data
	});
</script>