<?php
    $qry = $this->db->query("
        SELECT 		
		cedula,
		nombres,
		apellidos,
		sexo AS S,
		distrito,
		local,
		DATE_FORMAT(fecha_nac,'%d/%m/%Y') AS 'F.Nacimiento',
        YEAR(NOW()) - YEAR(fecha_nac) AS 'Edad'
		FROM padron
		WHERE MONTH(fecha_nac) = MONTH(NOW()) AND DAY(fecha_nac) > DAY(NOW())
		ORDER BY DAY(fecha_nac) ASC		
    ");
?>
<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-list"></i> Personas que cumplen año este mes</h5>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <!--<a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año Lectivo</b></a>
                            </li>                            
                        </ul>-->
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body"  style="height:350px; overflow:auto">
                <div class="widget-main no-padding">
                    <div class="widget-main no-padding">
                        <?php sqlToHtml($qry); ?>

                    </div>
                </div>
            </div>
</div>
<script>

</script>
