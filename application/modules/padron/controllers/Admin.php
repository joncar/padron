<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function padron(){
            $crud = $this->crud_function('','');
            if($crud->getParameters()=='list'){
                $crud->set_relation('semaforos_id','semaforos','color');
            }else{
                $crud->set_relation('semaforos_id','semaforos','denominacion');
            }

            $crud->display_as('semaforos_id','Tendencia')
                 ->columns('cedula','nombres','apellidos','sexo','fecha_nac','distrito','local','departamento','voto','semaforos_id');
            $crud->callback_column('sbb4c515d',function($val){
                return '<span style="display:inline-block; width:15px; height:15px; background:'.$val.'"></span>';
            });            
            $output = $crud->render();
            $this->loadView($output);
        }

        function semaforos(){
            $crud = $this->crud_function('','');
            $crud->callback_field('color',function($val){
                return '<input type="color" name="color" id="field-color" class="form-control" value="'.$val.'">';
            });
            $crud->callback_column('color',function($val){
                return '<span style="display:inline-block; width:15px; height:15px; background:'.$val.'"></span>';
            });
            $output = $crud->render();
            $this->loadView($output);
        }
    }
?>
